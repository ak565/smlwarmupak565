fun min (a: int, b:int) =
 case Int.compare (a,b) of
 EQUAL => a
 | LESS => a
 | GREATER => b

fun fib 1=1
| fib n = if n<1
           then 0
           else fib (n-1) + fib (n-2)

fun sumList ls = foldl (op +) 0 ls;

fun squareList ls = 
let fun sq x = x*x
in
 map sq ls
end

fun flatten l = foldr (op @) [] l;

fun mymap f l= 
let fun g (a,ls) = (f a)::ls
in
 foldr g [] l
end;

fun myfilter f [] = []
| myfilter f ls = 
let fun g (x,xs) = if (f x)
                   then x::xs
                   else xs
in
  foldr g [] ls
end;

fun count f [] = 0
| count f ls = 
let fun g (x,c) = if (f x)
                  then c+1
                  else c
in
  foldr g 0 ls
end;

fun partialMap f ls =   
let fun g (a, ls) = case (f a) of
NONE => ls
| SOME(b) => b::ls
in 
 foldr g [] ls
end;

functor F(M: ORD_MAP where type Key.ord_key = string) (S:ORD_SET where type Key.ord_key = string) :>

sig
 val proc : string list -> S.set M.map
end
=
struct
fun proc (filenames: string list) =
    let fun bulkRead filename = TextIO.inputAll(TextIO.openIn(filename))
                      fun f2w filename = String.tokens Char.isSpace (bulkRead filename)
		      fun pairUpHelper (f, []) = []
		      | pairUpHelper (f, w::ws) = (f, w)::pairUpHelper(f, ws)
		      fun pairUp file = pairUpHelper (file, f2w file)
		      fun addPair ((file, word), mapName) = if M.inDomain(mapName, word)
		                                            then M.insert(mapName, word, S.add(M.lookup(mapName, word), file))
							    else M.insert(mapName, word, S.singleton(file))
	              fun index (file, mapName) = let val pairs = pairUp file
		                                   in
						  foldl addPair mapName pairs
						  end

                  in
		   foldl index M.empty filenames
		  end
end

fun eval (NUM x) = x
| eval(PLUS(x,y)) = eval(x)+eval(y)
| eval(MINUS(x,y)) = eval(x)-eval(y)
| eval(TIMES(x,y)) = eval(x)*eval(y)
| eval(DIV(x,y)) = eval(x) div eval(y)
| eval(F(x, f)) = f (map eval x)

fun isPrime x = 
 let fun check y = 
  if x =2 then true
  else if  x mod y =0 then false
  else if y*y >=x then true
  else check (y+1)
 in
  check(2)
 end

